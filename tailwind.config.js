const { guessProductionMode } = require("@ngneat/tailwind");

process.env.TAILWIND_MODE = guessProductionMode() ? 'build' : 'watch';

module.exports = {
    prefix: '',
    mode: 'jit',
    purge: {
      content: [
        './src/**/*.{html,ts,css,scss,sass,less,styl}',
      ]
    },
    darkMode: 'class', // or 'media' or 'class'
    theme: {

     
      fontWeight: {
        hairline: 100,
        'extra-light': 100,
        thin: 200,
        light: 300,
        normal: 400,
        medium: 500,
        grande: 505,
        am: 50,
        semibold: 600,
        bold: 700,
        extrabold: 800,
        'extra-bold': 800,
        black: 900,
      },

      extend: {

        width: {
          '1/7': '14.2857143%',
          '2/7': '28.5714286%',
          '3/7': '42.8571429%',
          '4/7': '57.1428571%',
          '5/7': '71.4285714%',
          '6/7': '85.7142857%',
          '40':  '17.5rem',
          '99': '34.875rem',
          '99.4': '40.625',
          '100': '41.813rem',
          '110': '42rem',
          '120': '45rem',
          '128': '46.5rem',
          '130':'50rem',
          '132': '52rem',
          '134':'55rem' 
        },

        colors: {
          'plomo': {
              '200':  '#D0D0D0',
              '400': '#F6F6F6'
          },

          'azul':{
            '500': '#2895E5',
            '550': '#1491ED',
          }
        },

        fontFamily: {
          Manrope: ['Manrope']
        }

      },
    },
    variants: {
      extend: {},
    },
    plugins: [require('@tailwindcss/aspect-ratio'),require('@tailwindcss/forms'),require('@tailwindcss/line-clamp'),require('@tailwindcss/typography')],
};
